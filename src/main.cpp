// using Lorem impsum from https://satoristudio.net/delorean-ipsum/
//Program made by Pedro Soares on 2021

#include <iostream>
#include "StringList.hpp"

const int INPUT_COUNT = 5;
const std::string DeloreanIpsum = "Just {pop} anything, {pop}, say whatever's natural, the first thing that {pop} to your mind.\nTake that you {pop} son-of-a-bitch. My pine, why you.\nYou space bastard, you killed a pine.\nYou do? Yeah, it's 8:00.\nHey, {pop}, I thought I told you never to come in here.\nWell it's gonna cost you.\nHow much money you got on you?";
const std::string Delimiter = "{pop}";

void ReadInput(StringList *sList, const std::string &strOutput)
{
    std::string input = "";
    std::cout << strOutput << std::endl;
    std::cin >> input;
    std::cin.ignore(10000, '\n');
    sList->Push(input);
}

void PrintIpsum(std::string ipsum, const std::string &delimiter, StringList *sList)
{
    size_t position = 0;
    std::string strPart = "";
    while ((position = ipsum.find(delimiter)) != std::string::npos)
    {
        strPart = ipsum.substr(0, position);
        std::cout << strPart << "\033[31m" << sList->Pop() << "\033[39m";
        ipsum.erase(0, position + delimiter.length());
    }
    std::cout << ipsum << std::endl;
}

int main()
{
    std::cout << "Hello from Pedro's MadLib project!" << std::endl;
MADLIB_INIT:
    auto sList = new StringList();

    ReadInput(sList, "Enter a verb: ");
    ReadInput(sList, "Enter a noun: ");
    ReadInput(sList, "Enter a past tense verb: ");
    ReadInput(sList, "Enter an adjective: ");
    ReadInput(sList, "Enter another noun: ");

    PrintIpsum(DeloreanIpsum, Delimiter, sList);

    delete (sList);

TRY_AGAIN:
{
    std::cout << "Try again? Y/n" << std::endl;
    char x;
    std::cin >> x;
    std::cin.ignore(10000, '\n');

    if (x == 'Y')
        goto MADLIB_INIT;
    else if (x != 'n')
        goto TRY_AGAIN;
}
    return 0;
}