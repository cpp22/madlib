#include "StringList.hpp"

StringList::StringList()
{
}

StringList::~StringList()
{
    ClearList();
}

std::string StringList::FullList()
{
    std::string full_list = "";
    Node *ptr = top;
    while (ptr != NULL)
    {
        full_list += ptr->Data;
        ptr = ptr->Next;
    }
    return full_list;
}

void StringList::Push(std::string str)
{
    Node *ptr = new Node();
    ptr->Data = str;
    if (bottom != nullptr)
        bottom->Next = ptr;
    bottom = ptr;
    if (top == nullptr)
    {
        top = bottom;
    }
    count++;
}

std::string StringList::Showtop()
{
    if (IsEmpty())
    {
        return "Empty List";
    }
    return top->Data;
}

std::string StringList::Pop()
{
    if (IsEmpty())
    {
        return "List is Empty";
    }

    Node *ptr = top;
    top = top->Next;
    std::string str = ptr->Data;
    delete ptr;
    count--;
    return str;
}

void StringList::ClearList()
{
    while (IsEmpty() == false)
    {
        Pop();
    }
}
