# MadLib Project

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Project made for C++ Assignment 
Simple MadLib implementation  
compiled GCC version 11.1.0 (GCC) target: x86_64-pc-linux-gnu  
G++ compiler version c++17 

***

## Assignment Instructions: 

Write a program that includes the concepts of lists and pointers in order to demonstrate your understanding of the subject.  
Your assignment can be an implementation of the following specification, or a submission of equal or approximate complexity.  
Write a Mad Libs game  
* Loop the following until all words are collected
* Ask users for word type of the following options
    * [verb, noun, past tense verb, adjective, noun]
* User inputs word -> Add to list
* When all words are collected, output the final story
* Keep in mind how a sentence is defined in madlibs, where it is a container of other word types.  

Be careful about scope creep! The more creative you are, the more you're incrementing the amount of work to be done. 
Basic functionality + polish is the sweet spot here.  

***  

[![Buy Me A Coffee](https://cdn.buymeacoffee.com/buttons/v2/default-yellow.png)](https://www.buymeacoffee.com/PCtzonoes)



