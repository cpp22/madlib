#pragma once
#include <string>

class StringList
{
public:
    struct Node
    {
        std::string Data = "";
        Node *Next = nullptr;
    };

    bool IsEmpty()
    {
        return top == nullptr;
    }

    void Push(std::string valstrue);

    std::string Showtop();

    std::string Pop();

    std::string FullList();

    void ClearList();

    StringList();
    ~StringList();

private:
    Node *top = nullptr;
    Node *bottom = nullptr;
    int count = 0;
};